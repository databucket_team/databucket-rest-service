'use strict';

angular.module('DatabucketApp').controller('DatabucketController', ['$scope', 'DatabucketService', function($scope, DatabucketService) {
    var self = this;
    self.bundle={id:null,owner:'',tag:'',description:'',createdAt:'',createdBy:'',updatedAt:'',updatedBy:''};
    self.bundles=[];

    self.submit = submit;

    fetchAllUsers();

    function fetchTableBundles(tableName, tagName){
        DatabucketService.fetchTableBundles(tableName, tagName)
            .then(
            function(d) {
                self.bundles = d;
            },
            function(errResponse){
                console.error('Error while fetching Bundles');
            }
        );
    }

}]);
