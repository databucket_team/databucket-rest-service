'use strict';

angular.module('DatabucketApp').factory('DatabucketService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8181/DatabucketWeb/';

    var factory = {
        fetchTableBundles: fetchTableBundles
    };

    return factory;

    function fetchTableBundles(tableName, tagName) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + tableName + tagName)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Bundles');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
}]);
