<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Databucket</title>  
    <style>
      .username.ng-valid {
          background-color: lightgreen;
      }
      .username.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .username.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

      .email.ng-valid {
          background-color: lightgreen;
      }
      .email.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .email.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="DatabucketApp" class="ng-cloak">
  	<p>Welcome to Databucket rest service. To show all available methods sets this url with <b>api/application.wadl</b> ending.
  	  <!--
      <div class="generic-container" ng-controller="DatabucketController as ctrl">
          <div class="panel panel-default">
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Id</th>
                              <th>Owner</th>
                              <th>Tag</th>
                              <th>Description</th>
                              <th>Created at</th>
                              <th>Created by</th>
                              <th>Updated at</th>
                              <th>Updated by</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="b in ctrl.bundles">
                              <td><span ng-bind="b.id"></span></td>
                              <td><span ng-bind="b.owner"></span></td>
                              <td><span ng-bind="b.tag"></span></td>
                              <td><span ng-bind="b.description"></span></td>
                              <td><span ng-bind="b.createdAt"></span></td>
                              <td><span ng-bind="b.createdBy"></span></td>
                              <td><span ng-bind="b.updatedAt"></span></td>
                              <td><span ng-bind="b.updatedBy"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      -->
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script src="<c:url value='/static/js/service/databucket_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/databucket_controller.js' />"></script>
  </body>
</html>