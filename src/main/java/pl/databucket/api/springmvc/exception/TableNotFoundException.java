package pl.databucket.api.springmvc.exception;

public class TableNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TableNotFoundException() {}
	
	public TableNotFoundException(String message) {
		super(message);
	}

}
