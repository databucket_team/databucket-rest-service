package pl.databucket.api.springmvc.dao;

import javax.sql.DataSource;

import pl.databucket.api.springmvc.exception.TableNotFoundException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DatabucketDAOImpl implements DatabucketService {

	private DataSource datasource;

	public void setDataSource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	@Override
	public boolean createTable(String tableName) {
		if (!tableName.startsWith("_")) {
	    	String sqlQuery = "CREATE TABLE `" + tableName + "` (" +
					  "`id_bundle` INT(11) NOT NULL AUTO_INCREMENT," +
					  "`id_tag` INT(5) NOT NULL," +
					  "`owner` VARCHAR(100) NULL DEFAULT NULL," +
					  "`description` VARCHAR(500) NOT NULL DEFAULT ''," +
					  "`properties` TEXT DEFAULT NULL," +
					  "`created_at` DATETIME NOT NULL DEFAULT NOW()," +
					  "`created_by` VARCHAR(100) NOT NULL," +
					  "`updated_at` DATETIME NULL DEFAULT NULL," +
					  "`updated_by` VARCHAR(100) NULL DEFAULT NULL," +
					  "`locked` INT(1) NOT NULL DEFAULT '0'," +
					  "PRIMARY KEY (`id_bundle`)," +
					  "KEY `id_tag_idx` (`id_tag`)," +
					  "INDEX `locked_idx` (`locked` ASC)," + 
					  "CONSTRAINT `" + tableName + "_id_tag` FOREIGN KEY (`id_tag`) REFERENCES `_tags` (`id_tag`) ON DELETE NO ACTION ON UPDATE NO ACTION) " +
					"AUTO_INCREMENT = 1 " +
					"DEFAULT CHARACTER SET = utf8;";		
			
	    	try {
	    		Connection conn = datasource.getConnection();
				Statement stmtStatus = conn.createStatement();
				stmtStatus.execute(sqlQuery);
			    stmtStatus.close();
			    	    
			    // Create trigger
			    sqlQuery = " CREATE TRIGGER `" + tableName + "_BEFORE_UPDATE` BEFORE UPDATE ON `" + tableName + "`\n" + 
						"FOR EACH ROW\n" +
						"BEGIN\n" +
						"	SET NEW.updated_at = NOW();\n" +
						"END";
			    stmtStatus = conn.createStatement();
				stmtStatus.execute(sqlQuery);
			    stmtStatus.close();
			    return true;
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    	}
    	}
		return false;
	}

	@Override
	public boolean deleteTable(String tableName) {
		try {
			Connection conn = datasource.getConnection();
	    	String sqlQuery = "DROP TABLE `" + tableName + "`";
			Statement stmtStatus = conn.createStatement();
			stmtStatus.executeUpdate(sqlQuery);
		    stmtStatus.close();
		    
		    // Remove table tags
		    sqlQuery = "delete from _tags where table_name = '" + tableName + "'";
	
			Statement stat = conn.createStatement();
			stat.execute(sqlQuery);
			stat.close();
			return true;
    	} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<String> getTables() {
		String sql = "show tables";
		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			List<String> tables = new ArrayList<String>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (!rs.getString(1).equals("_tags"))
					tables.add(rs.getString(1));
			}
			rs.close();
			ps.close();
			return tables;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	@Override
	public List<String> getTableTags(String tableName) {
		String sql = "select tag_name from _tags where table_name = ?";
		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, tableName);
			List<String> tags = new ArrayList<String>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				tags.add(rs.getString("tag_name"));
			}
			rs.close();
			ps.close();
			return tags;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	@Override
	public List<Bundle> getTableBundles(String tableName) throws TableNotFoundException {
		String sql = "select tab.id_bundle, tab.id_tag, (select tag.tag_name from _tags tag where tag.id_tag = tab.id_tag) as 'tag_name', tab.owner, tab.description, tab.properties from `" + tableName + "` tab";

		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			List<Bundle> bundles = new ArrayList<Bundle>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Bundle bundle = new Bundle(tableName, rs.getString("tag_name"), rs.getInt("id_tag"));
				bundle.setBundleId(rs.getLong("id_bundle"));
				//bundle.setOwner(rs.getString("owner"));
				bundle.setDescription(rs.getString("description"));
				bundle.setProperties(stringToList(rs.getString("properties")));
				bundles.add(bundle);
			}
			rs.close();
			ps.close();
			return bundles;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	@Override
	public List<Bundle> getTagBundles(String tableName, int tagId) throws TableNotFoundException {
		String sql = "select tab.id_bundle, (select tag.tag_name from _tags tag where tag.id_tag = tab.id_tag) as 'tag_name', tab.owner, tab.description, tab.properties from `" + tableName + "` tab where tab.id_tag = ?";

		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, tagId);
			List<Bundle> bundles = new ArrayList<Bundle>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Bundle bundle = new Bundle(tableName, rs.getString("tag_name"), tagId);
				bundle.setBundleId(rs.getLong("id_bundle"));
				//bundle.setOwner(rs.getString("owner"));
				bundle.setDescription(rs.getString("description"));
				bundle.setProperties(stringToList(rs.getString("properties")));
				bundles.add(bundle);
			}
			rs.close();
			ps.close();
			return bundles;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	@Override
	public List<Bundle> getTagBundles(String tableName, String tagName) throws TableNotFoundException {
		int tagId = getTagId(tableName, tagName);
		return getTagBundles(tableName, tagId);
	}

	private int getTagId(String tableName, String tagName) {
		String sql = "select id_tag from _tags where table_name = ? and tag_name = ?";
		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, tableName);
			ps.setString(2, tagName);
			int tagId = -1;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				tagId = rs.getInt("id_tag");
			}
			rs.close();
			ps.close();
			return tagId;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	@Override
	public long addBundle(Bundle bundle) throws TableNotFoundException {
		Connection conn = null;

		try {
			conn = datasource.getConnection();
			// Add new tag
	    	if (bundle.getTagId() < 0) 
	    		bundle.setTagNameAndId(bundle.getTagName(), createTag(bundle.getTableName(), bundle.getTagName()));
	    	
	    	Long bundleId = -1l;
	
	        String sqlQuery = "insert into `" + bundle.getTableName() + "` (id_tag, description, owner, properties, created_by, locked) values (?, ?, ?, ?, ?, ?)";
	
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	        prepStat.setInt(1, bundle.getTagId());
	        if (bundle.getDescription() != null)
	        	prepStat.setString(2, bundle.getDescription());
	        else 
	        	prepStat.setNull(2, Types.VARCHAR);
	        	
	        if (bundle.isLocked())
	            prepStat.setString(3, bundle.getOwner());
	        else
	            prepStat.setNull(3, Types.VARCHAR);
	
	        if (bundle.getProperties() != null && bundle.getProperties().size() > 0)
	            prepStat.setString(4, listToString(bundle.getProperties()));
	        else
	            prepStat.setString(4, "");
	
	        prepStat.setString(5, bundle.getOwner());
	        prepStat.setInt(6, bundle.isLocked() ? 1 : 0);
	        prepStat.executeUpdate();
	        ResultSet resultSet = prepStat.getGeneratedKeys();
	        if (resultSet.next())
	            bundleId = resultSet.getLong(1);
	        resultSet.close();
	        prepStat.close();   	
	    	
	    	bundle.setBundleId(bundleId);
	    	return bundleId;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	@Override
	public Bundle lockBundle(String userName, String tableName, String tagName) throws TableNotFoundException {		
		int tagId = getTagId(tableName, tagName);
		return lockBundle(userName, tableName, tagId);		
	}

	@Override
	public Bundle lockBundle(String userName, String tableName, int tagId) throws TableNotFoundException {
		int bundleId = -1;
		Connection conn = null;
		try {
			conn = datasource.getConnection();
			CallableStatement callStat = conn.prepareCall("{call getAndLockBundle(?, ?, ?, ?)}");
			callStat.setString(1, userName);
			callStat.setString(2, tableName);
			callStat.setInt(3, tagId);
			callStat.registerOutParameter(4, Types.INTEGER);
			callStat.execute();
			bundleId = callStat.getInt(4);
			callStat.close();
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
		
		if (bundleId > 0)
			return getBundleById(tableName, bundleId);
		else
			return null;
	}

	@Override
	public Bundle lockRandomBundle(String userName, String tableName, String tagName) throws TableNotFoundException {
		int tagId = getTagId(tableName, tagName);
		return lockRandomBundle(userName, tableName, tagId);
	}

	@Override
	public Bundle lockRandomBundle(String userName, String tableName, int tagId) throws TableNotFoundException {
		int bundleId = -1;
		Connection conn = null;
		try {
			conn = datasource.getConnection();
			CallableStatement callStat = conn.prepareCall("{call getAndLockRandomBundle(?, ?, ?, ?)}");
	        callStat.setString(1, userName);
	        callStat.setString(2, tableName);
	        callStat.setInt(3, tagId);
	        callStat.registerOutParameter(4, Types.INTEGER);
	        callStat.execute();
	        bundleId = callStat.getInt(4);
	        callStat.close();
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
		
		if (bundleId > 0)
			return getBundleById(tableName, bundleId);
		else
			return null;
	}

	@Override
	public Bundle getBundleById(String tableName, long bundleId) throws TableNotFoundException {
		String query = "select t.id_bundle, t.id_tag, (select t2.tag_name from _tags t2 where t2.id_tag = t.id_tag) as tag_name, t.owner, t.description, t.properties, t.locked from `" + tableName + "` t where t.id_bundle = " + bundleId;
		
		Bundle bundle = null;

		Connection conn = null;
		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				bundle = new Bundle(tableName, rs.getString("tag_name"), rs.getInt("id_tag"));
				bundle.setBundleId(rs.getLong("id_bundle"));
				bundle.setOwner(rs.getString("owner"));
				bundle.setDescription(rs.getString("description"));
				bundle.setProperties(stringToList(rs.getString("properties")));
			}
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
		return bundle;
	}

	// Get bundles
	@Override
	public List<Bundle> lockBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException {
		int tagId = getTagId(tableName, tagName);
		return lockBundles(userName, tableName, tagId, count);
	}

	@Override
	public List<Bundle> lockBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException {
		Connection conn = null;
		String bundleIdsStr = null;
		try {
			conn = datasource.getConnection();
			CallableStatement callStat = conn.prepareCall("{call getAndLockBundles(?, ?, ?, ?, ?)}");
	        callStat.setString(1, userName);
	        callStat.setString(2, tableName);
	        callStat.setInt(3, tagId);
	        callStat.setInt(4, count);
	        callStat.registerOutParameter(5, Types.VARCHAR);
	        callStat.execute();
	        bundleIdsStr = callStat.getString(5);
	        callStat.close();
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
        
        if (bundleIdsStr != null)
        	return getBundlesByIds(tableName, bundleIdsStr);
        else
        	return null;
	}

	@Override
	public List<Bundle> lockRandomBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException {
		int tagId = getTagId(tableName, tagName);
		return lockRandomBundles(userName, tableName, tagId, count);
	}

	@Override
	public List<Bundle> lockRandomBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException {
		Connection conn = null;
		String bundleIdsStr = null;
		try {
			conn = datasource.getConnection();
			CallableStatement callStat = conn.prepareCall("{call getAndLockRandomBundles(?, ?, ?, ?, ?)}");
	        callStat.setString(1, userName);
	        callStat.setString(2, tableName);
	        callStat.setInt(3, tagId);
	        callStat.setInt(4, count);
	        callStat.registerOutParameter(5, Types.VARCHAR);
	        callStat.execute();
	        bundleIdsStr = callStat.getString(5);
	        callStat.close();
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
        
        if (bundleIdsStr != null)
        	return getBundlesByIds(tableName, bundleIdsStr);
        else
        	return null;
	}

	@Override
	public List<Bundle> getBundlesByIds(String tableName, String bundleIdsStr) throws TableNotFoundException {
		if (bundleIdsStr != null && bundleIdsStr.length() > 0) {
			List<Bundle> bundles = new ArrayList<Bundle>();

			String query = "select t.id_bundle, t.id_tag, (select t2.tag_name from _tags t2 where t2.id_tag = t.id_tag) as tag_name, t.owner, t.description, t.properties, t.locked from `" + tableName + "` t where t.id_bundle in (" + bundleIdsStr + ")";

			Connection conn = null;
			try {
				conn = datasource.getConnection();
				PreparedStatement ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Bundle bundle = new Bundle(tableName, rs.getString("tag_name"), rs.getInt("id_tag"));
					bundle.setBundleId(rs.getLong("id_bundle"));
					bundle.setOwner(rs.getString("owner"));
					bundle.setDescription(rs.getString("description"));
					bundle.setProperties(stringToList(rs.getString("properties")));
					bundle.setLocked(rs.getBoolean("locked"));
					bundles.add(bundle);
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				if (e.getMessage().matches("Table '.*' doesn't exist"))
					throw new TableNotFoundException(e.getMessage());
				else
					throw new RuntimeException(e);
			} finally {
				if (conn != null)
					try {
						conn.close();
					} catch (Exception ignore) {
					}
			}
			return bundles;
		}

		return null;
	}
	
	public List<Bundle> getBundlesByProperties(String tableName, Map<String, String> properties) throws TableNotFoundException {
		if (tableName != null && properties != null) {
            
            String query = "select t.id_bundle, t.id_tag, (select t2.tag_name from _tags t2 where t2.id_tag = t.id_tag) as tag_name, t.owner, t.description, t.properties, t.locked from `" + tableName + "` t where" + getGenerateSqlRestrictions(properties, "t");
            
            List<Bundle> bundles = new ArrayList<Bundle>();
            Connection conn = null;
			try {
				conn = datasource.getConnection();
	            
	            PreparedStatement ps = conn.prepareStatement(query);
	            ResultSet rs = ps.executeQuery();
	            while (rs.next()) {         
	                Bundle bundle = new Bundle(tableName, rs.getString("tag_name"), rs.getInt("id_tag"));
	                bundle.setBundleId(rs.getLong("id_bundle"));
	                bundle.setOwner(rs.getString("owner"));
	                bundle.setDescription(rs.getString("description"));
	                bundle.setProperties(stringToList(rs.getString("properties")));
	                bundle.setLocked(rs.getBoolean("locked"));
	                bundles.add(bundle);
	            }
	            rs.close();
	            ps.close();
	            return bundles;
			} catch (SQLException e) {
				if (e.getMessage().matches("Table '.*' doesn't exist"))
					throw new TableNotFoundException(e.getMessage());
				else
					throw new RuntimeException(e);
			} finally {
				if (conn != null)
					try {
						conn.close();
					} catch (Exception ignore) {
					}
			}	
		}
		return null;
	}
	
	public List<Bundle> getBundlesByProperties(String tableName, String tagName, Map<String, String> properties) throws TableNotFoundException {
		if (tableName != null && properties != null) {
			int tagId = getTagId(tableName, tagName);
            
            String query = "select t.id_bundle, t.id_tag, (select t2.tag_name from _tags t2 where t2.id_tag = t.id_tag) as tag_name, t.owner, t.description, t.properties, t.locked from `" + tableName + "` t where t.id_tag = " + tagId + " and " + getGenerateSqlRestrictions(properties, "t");
            
            List<Bundle> bundles = new ArrayList<Bundle>();
            Connection conn = null;
			try {
				conn = datasource.getConnection();
	            
	            PreparedStatement ps = conn.prepareStatement(query);
	            ResultSet rs = ps.executeQuery();
	            while (rs.next()) {         
	                Bundle bundle = new Bundle(tableName, rs.getString("tag_name"), rs.getInt("id_tag"));
	                bundle.setBundleId(rs.getLong("id_bundle"));
	                bundle.setOwner(rs.getString("owner"));
	                bundle.setDescription(rs.getString("description"));
	                bundle.setProperties(stringToList(rs.getString("properties")));
	                bundle.setLocked(rs.getBoolean("locked"));
	                bundles.add(bundle);
	            }
	            rs.close();
	            ps.close();
	            return bundles;
			} catch (SQLException e) {
				if (e.getMessage().matches("Table '.*' doesn't exist"))
					throw new TableNotFoundException(e.getMessage());
				else
					throw new RuntimeException(e);
			} finally {
				if (conn != null)
					try {
						conn.close();
					} catch (Exception ignore) {
					}
			}	
		}
		return null;
	}
	
	/**
     * Generates sql 'where' restrictions for array of properties
     * @param properties - pairs of properties name and value
     * @param alias - table alias (not obligatory, could be null)
     * @return String (eg. properties like '%propName":"propValue% and properties like '%propName":"propValue%
     */
    private String getGenerateSqlRestrictions(Map<String, String> properties, String alias) {
    	String restrictions = " ";
    	
    	Iterator<Map.Entry<String, String>> it = properties.entrySet().iterator();
    	while (it.hasNext()) {
    		Map.Entry<String, String> pair = (Map.Entry<String, String>) it.next();
    		if (alias != null)
				restrictions += alias + ".properties like '%" + pair.getKey() + "\":\"" + pair.getValue() + "%' and ";
			else
				restrictions += "properties like '%" + pair.getKey() + "\":\"" + pair.getValue() + "%' and ";
    	}
    	
		return restrictions.substring(0, restrictions.length() - 5);
    }

	// Update bundle
	@Override
	public boolean saveBundle(Bundle bundle) throws TableNotFoundException {
    	if (bundle.getTagId() < 0)
    		bundle.setTagId(createTag(bundle.getTableName(), bundle.getTagName()));
    	
        String sqlQuery = "update `" + bundle.getTableName() + "` set id_tag = ?, description = ?, updated_by = ?, properties = ? where id_bundle = ?";
        Connection conn = null;
		try {
			conn = datasource.getConnection();
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery);
	        prepStat.setInt(1, bundle.getTagId());
	        prepStat.setString(2, bundle.getDescription());
	        prepStat.setString(3, bundle.getOwner());
	        prepStat.setString(4, listToString(bundle.getProperties()));
	        prepStat.setLong(5, bundle.getBundleId());
	        prepStat.execute();
	        prepStat.close();
	        return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}        
	}
	
	@Override
	public boolean saveBundleTagById(Bundle bundle) throws TableNotFoundException {
		String sqlQuery = "update `" + bundle.getTableName() + "` set id_tag = ?, updated_by = ? where id_bundle = ?";

		Connection conn = null;
		try {
			conn = datasource.getConnection();
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery);
	        prepStat.setInt(1, bundle.getTagId());
	        prepStat.setString(2, bundle.getOwner());
	        prepStat.setLong(3, bundle.getBundleId());
	        prepStat.execute();
	        prepStat.close();
	        return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
	}

	@Override
	public boolean saveBundleTagByName(Bundle bundle) throws TableNotFoundException {
		int tagId = createTag(bundle.getTableName(), bundle.getTagName());
    	bundle.setTagId(tagId);
    	return saveBundleTagById(bundle);
	}

	@Override
	public boolean saveBundleDescription(Bundle bundle) {
		String sqlQuery = "update `" + bundle.getTableName() + "` set description = ?, updated_by = ? where id_bundle = ?";

		Connection conn = null;
		try {
			conn = datasource.getConnection();
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery);
	        prepStat.setString(1, bundle.getDescription());
	        prepStat.setString(2, bundle.getOwner());
	        prepStat.setLong(3, bundle.getBundleId());
	        prepStat.execute();
	        prepStat.close();
	        return true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
	}

	@Override
	public boolean unlockBundle(Bundle bundle) throws TableNotFoundException {
		String sqlQuery = "update `" + bundle.getTableName() + "` set owner = ?, locked = ?, updated_by = ? where id_bundle = ?";

		Connection conn = null;
		try {
			conn = datasource.getConnection();
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery);
	        prepStat.setNull(1, Types.VARCHAR);
	        prepStat.setBoolean(2, false);
	        prepStat.setString(3, bundle.getOwner());
	        prepStat.setLong(4, bundle.getBundleId());
	        prepStat.execute();
	        prepStat.close();
	        return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
	}

	@Override
	public boolean saveBundleProperties(Bundle bundle) throws TableNotFoundException {
		String sqlQuery = "update `" + bundle.getTableName() + "` set updated_by = ?, properties = ? where id_bundle = ?";

		Connection conn = null;
		try {
			conn = datasource.getConnection();
	        PreparedStatement prepStat = conn.prepareStatement(sqlQuery);
	        prepStat.setString(1, bundle.getOwner());
	        prepStat.setString(2, listToString(bundle.getProperties()));
	        prepStat.setLong(3, bundle.getBundleId());
	        prepStat.execute();
	        prepStat.close();
	        return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
	}

	/**
     * Removes the bundle from the database table
     * @param tableName - the table name
     * @param bundleId - the bundle id
	 * @throws TableNotFoundException 
     * @throws SQLException 
     */
	@Override
    public boolean deleteBundleById(String tableName, long bundleId) throws TableNotFoundException {
    	String sqlQuery = "delete from `" + tableName + "` where id_bundle = " + bundleId;

    	Connection conn = null;
		try {
			conn = datasource.getConnection();
			Statement stat = conn.createStatement();
			stat.execute(sqlQuery);
			stat.close();
			return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
    }
	
	@Override
    public boolean deleteBundlesByIds(String tableName, String bundlesIds) throws TableNotFoundException {
    	String sqlQuery = "delete from `" + tableName + "` where id_bundle in (" + bundlesIds + ")";

    	Connection conn = null;
		try {
			conn = datasource.getConnection();
			Statement stat = conn.createStatement();
			stat.execute(sqlQuery);
			stat.close();
			return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
    }
    
    /**
     * Removes bundles.
     * @param tableName - the table name
     * @param bundlesIds - the list of bundles ids
     * @throws TableNotFoundException 
     * @throws SQLException
     */
    public boolean deleteBundles(String tableName, List<Long> bundlesIds) throws TableNotFoundException {
		String bundlesIdsStr = "";
		for (Long bundleId : bundlesIds)
			bundlesIdsStr += bundleId + ",";	    	
    	String sqlQuery = "delete from `" + tableName + "` where id_bundle in (" + bundlesIdsStr + ")";

    	Connection conn = null;
		try {
			conn = datasource.getConnection();
			Statement stat = conn.createStatement();
			stat.execute(sqlQuery);
			stat.close();
			return true;
		} catch (SQLException e) {
			if (e.getMessage().matches("Table '.*' doesn't exist"))
				throw new TableNotFoundException(e.getMessage());
			else
				throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception ignore) {
				}
		}
    }
	
	/**
     * If the tag does not exist then creates new tag. If the tag exists, returns it id.
     * @param tableName - the source table name
     * @param tagName - the tag name
     * @return the tag id
     * @throws SQLException 
     * @throws Exception
     */
    private int createTag(String tableName, String tagName) {
        // Check if the tag exists
        int tagId = getTagId(tableName, tagName);

        // Add a new tag for the table
        if (tagId < 0) {
        	Connection conn = null;
			try {
				conn = datasource.getConnection();
	            String sqlQuery = "insert into _tags (table_name, tag_name) VALUES ('" + tableName + "', '" + tagName + "')";
	            Statement stmtStatus = conn.createStatement();
	            stmtStatus.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	            ResultSet resultSet = stmtStatus.getGeneratedKeys();
	            if (resultSet.next())
	                tagId = resultSet.getInt(1);
	            resultSet.close();
	            stmtStatus.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				if (conn != null)
					try {
						conn.close();
					} catch (Exception ignore) {
					}
			}	            
        }

        return tagId;
    }
	
	/**
     * Converts the list of properties into string
     * @param properties - the list of properties
     * @return string
     */
    private String listToString(HashMap<String, String> properties) {    	
        if (properties.size() > 0) {
        	Iterator<Entry<String, String>> iterator = properties.entrySet().iterator();
            String result = "[";
            while(iterator.hasNext()) {
            	Map.Entry<String, String> property = (Map.Entry<String, String>)iterator.next();
            	result += "{\"" + escapeJson(property.getKey()) + "\":\"" + escapeJson(property.getValue()) + "\"}";
            }
            result = result.replace("}{", "},{") + "]";
            return result;
        } else
            return null;
    }

    /**
     * Converts the string into list of properties
     * @param jSonProps - the string contains properties
     * @return the list of parameters
     */
    private HashMap<String, String> stringToList(String jSonProps) {
        if (jSonProps != null && jSonProps.length() > 10) {
            HashMap<String, String> params = new HashMap<String, String>();
            //[{"name1":"value1"},{"name2":"value2"}]
            String[] pairs = jSonProps.substring(3, jSonProps.length() - 3).split("\\\"\\},\\{\\\"");
            for (String pairsItem : pairs) {//name1":"value1
                String[] pair = pairsItem.split("\":\"");
                if (pair.length == 2)
                	params.put(unescapeJson(pair[0]), unescapeJson(pair[1]));
                else
                	params.put(unescapeJson(pair[0]), "");
            }
            return params;
        } else
            return null;
    }

    /**
     * Escapes parenthesis
     * @param str - the destination string
     * @return String
     */
    private String escapeJson(String str) {
        str = str.replace("{", "\\{");
        str = str.replace("}", "\\}");
        return str;
    }

    /**
     * Unecapses parenthesis
     * @param str - the destination string
     * @return String
     */
    private String unescapeJson(String str) {
        str = str.replace("\\{", "{");
        str = str.replace("\\}", "}");
        return str;
    }

}
