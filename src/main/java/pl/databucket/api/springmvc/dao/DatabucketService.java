package pl.databucket.api.springmvc.dao;

import java.util.List;
import java.util.Map;

import pl.databucket.api.springmvc.exception.TableNotFoundException;


public interface DatabucketService {

	public boolean createTable(String tableName);
	public boolean deleteTable(String tableName);
	public List<String> getTables();
	public List<String> getTableTags(String tableName);
	public List<Bundle> getTableBundles(String tableName) throws TableNotFoundException;
	public List<Bundle> getTagBundles(String tableName, String tagName) throws TableNotFoundException;
	public List<Bundle> getTagBundles(String tableName, int tagId) throws TableNotFoundException;
	
	// Add bundle
	public long addBundle(Bundle bundle) throws TableNotFoundException;
	
	// Get bundle
	public Bundle lockBundle(String userName, String tableName, String tagName) throws TableNotFoundException;
	public Bundle lockBundle(String userName, String tableName, int tagId) throws TableNotFoundException;		
	public Bundle lockRandomBundle(String userName, String tableName, String tagName) throws TableNotFoundException;
	public Bundle lockRandomBundle(String userName, String tableName, int tagId) throws TableNotFoundException;		
	public Bundle getBundleById(String tableName, long bundleId) throws TableNotFoundException;
		
	// Get bundles
	public List<Bundle> lockBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException;
	public List<Bundle> lockBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException;		
	public List<Bundle> lockRandomBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException;
	public List<Bundle> lockRandomBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException;		
	public List<Bundle> getBundlesByIds(String tableName, String bundleIdsStr) throws TableNotFoundException;
	public List<Bundle> getBundlesByProperties(String tableName, Map<String, String> properties) throws TableNotFoundException;
	public List<Bundle> getBundlesByProperties(String tableName, String tagName, Map<String, String> properties) throws TableNotFoundException;
		
	// Update bundle
	public boolean saveBundle(Bundle bundle) throws TableNotFoundException;
	public boolean saveBundleTagById(Bundle bundle) throws TableNotFoundException;
	public boolean saveBundleTagByName(Bundle bundle) throws TableNotFoundException;
	public boolean saveBundleDescription(Bundle bundle) throws TableNotFoundException;
	public boolean unlockBundle(Bundle bundle) throws TableNotFoundException;
	public boolean saveBundleProperties(Bundle bundle) throws TableNotFoundException;
		
	// Remove bundle
	public boolean deleteBundleById(String tableName, long bundleId) throws TableNotFoundException;
	public boolean deleteBundlesByIds(String tableName, String bundlesIds) throws TableNotFoundException;
}
