package pl.databucket.api.springmvc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import pl.databucket.api.springmvc.exception.TableNotFoundException;

@Service("databucketService")
public class DatabucketServiceImpl implements DatabucketService {
	
	ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
	DatabucketService databucketDAO = (DatabucketService) context.getBean("databucketDAO");

	@Override
	public List<String> getTables() {
		return databucketDAO.getTables();
	}

	@Override
	public List<String> getTableTags(String tableName) {
		return databucketDAO.getTableTags(tableName);
	}

	@Override
	public List<Bundle> getTableBundles(String tableName) throws TableNotFoundException {
		return databucketDAO.getTableBundles(tableName);
	}
	
	@Override
	public List<Bundle> getTagBundles(String tableName, int tagId) throws TableNotFoundException {
		return databucketDAO.getTagBundles(tableName, tagId);
	}

	@Override
	public List<Bundle> getTagBundles(String tableName, String tagName) throws TableNotFoundException {
		return databucketDAO.getTagBundles(tableName, tagName);
	}
	
	// Insert bundle
	@Override
	public long addBundle(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.addBundle(bundle);
	}
	
	// Get bundle
	@Override
	public Bundle lockBundle(String userName, String tableName, String tagName) throws TableNotFoundException {
		return databucketDAO.lockBundle(userName, tableName, tagName);
	}
	
	@Override
	public Bundle lockBundle(String userName, String tableName, int tagId) throws TableNotFoundException {
		return databucketDAO.lockBundle(userName, tableName, tagId);
	}
	
	@Override
	public Bundle lockRandomBundle(String userName, String tableName, String tagName) throws TableNotFoundException {
		return databucketDAO.lockRandomBundle(userName, tableName, tagName);
	}
	
	@Override
	public Bundle lockRandomBundle(String userName, String tableName, int tagId) throws TableNotFoundException {
		return databucketDAO.lockRandomBundle(userName, tableName, tagId);
	}
	
	@Override
	public Bundle getBundleById(String tableName, long bundleId) throws TableNotFoundException {
		return databucketDAO.getBundleById(tableName, bundleId);
	}
	
	// Get bundles
	@Override
	public List<Bundle> lockBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException {
		return databucketDAO.lockBundles(userName, tableName, tagName, count);
	}
	
	@Override
	public List<Bundle> lockBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException {
		return databucketDAO.lockBundles(userName, tableName, tagId, count);
	}
	
	@Override
	public List<Bundle> lockRandomBundles(String userName, String tableName, String tagName, int count) throws TableNotFoundException {
		return databucketDAO.lockRandomBundles(userName, tableName, tagName, count);
	}
	
	@Override
	public List<Bundle> lockRandomBundles(String userName, String tableName, int tagId, int count) throws TableNotFoundException {
		return databucketDAO.lockRandomBundles(userName, tableName, tagId, count);
	}
	
	@Override
	public List<Bundle> getBundlesByIds(String tableName, String bundleIdsStr) throws TableNotFoundException {
		return databucketDAO.getBundlesByIds(tableName, bundleIdsStr);
	}
	
	@Override
	public List<Bundle> getBundlesByProperties(String tableName, Map<String, String> properties) throws TableNotFoundException {
		return databucketDAO.getBundlesByProperties(tableName, properties);
	}
	
	@Override
	public List<Bundle> getBundlesByProperties(String tableName, String tagName, Map<String, String> properties) throws TableNotFoundException {
		return databucketDAO.getBundlesByProperties(tableName, tagName, properties);
	}
	
	// Update bundle
	@Override
	public boolean saveBundle(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.saveBundle(bundle);
	}
	
	@Override
	public boolean saveBundleTagById(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.saveBundleTagById(bundle);
	}
	
	@Override
	public boolean saveBundleTagByName(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.saveBundleTagByName(bundle);
	}
	
	@Override
	public boolean saveBundleDescription(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.saveBundleDescription(bundle);
	}
	
	@Override
	public boolean unlockBundle(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.unlockBundle(bundle);
	}
	
	@Override
	public boolean saveBundleProperties(Bundle bundle) throws TableNotFoundException {
		return databucketDAO.saveBundleProperties(bundle);
	}
	
	// Remove bundle
	@Override
	public boolean deleteBundleById(String tableName, long bundleId) throws TableNotFoundException {
		return databucketDAO.deleteBundleById(tableName, bundleId);
	}
	
	@Override
	public boolean deleteBundlesByIds(String tableName, String bundlesIds) throws TableNotFoundException {
		return databucketDAO.deleteBundlesByIds(tableName, bundlesIds);
	}

	@Override
	public boolean createTable(String tableName) {
		return databucketDAO.createTable(tableName);
	}

	@Override
	public boolean deleteTable(String tableName) {
		return databucketDAO.deleteTable(tableName);
	}	
}
