package pl.databucket.api.springmvc.dao;

import java.util.HashMap;

/**
 * The class object represents one data row from databucket database
 * Created by Krzysztof Słysz on 08.12.16.
 */
public class Bundle {

	/**
	 * The source table name
	 */
    private String tableName;
    
    /**
     * The source tag name
     */
    private String tagName;
    
    /**
     * The bundle id
     */
    private long bundleId = -1;
    
    /**
     * The source tag id
     */
    private int tagId = -1;
    
    /**
     * The bundle owner name
     */
    private String owner;
    
    /**
     * The bundle description
     */
    private String description;
    
    /**
     * The bundle state locked/unlocked (assigned to user/not assigned to user)
     */
    private boolean locked;
    
    /**
     * The bundle list of properties
     */
    private HashMap<String, String> properties;
    
    /**
     * Constructor
     * @param tableName - the bundle source table name
     */
    public Bundle(String tableName, String tagName) {
    	this.tableName = tableName;
    	this.tagName = tagName;
    	properties = new HashMap<String, String>();
    }
    
    /**
     * Constructor
     * @param tableName
     * @param tagId
     */
    public Bundle(String tableName, int tagId) {
    	this.tableName = tableName;
    	this.tagId = tagId;
    	properties = new HashMap<String, String>();
    }
    
    /**
     * Constructor - used by Databucket object
     * @param tableName - the bundle source table name
     * @param tagName - the bundle tag name
     * @param tagId - the bundle tag id
     */
    public Bundle(String tableName, String tagName, int tagId) {
    	this.tableName = tableName;
    	this.tagName = tagName;
    	this.tagId = tagId;
    	properties = new HashMap<String, String>();
    }
    
    /**
     * Constructor
     * @param tableName - the table name
     * @param bundleId - the bundle id
     */
    public Bundle(String tableName, long bundleId) {
    	this.tableName = tableName;
    	this.bundleId = bundleId;
    	properties = new HashMap<String, String>();
    }

    /**
     * Returns the source table name
     * @return String
     */
    public String getTableName() {
        return tableName;
    }
    
    /**
     * Sets both the tag name and id
     * @param tagName - the tag name
     * @param tagId - the tag id
     * @return self
     */
    public Bundle setTagNameAndId(String tagName, int tagId) {
    	this.tagName = tagName;
    	this.tagId = tagId;
    	return this;
    }
    
    /**
     * Sets the bundle tag name
     * @param tagName
     * @return
     */
    public Bundle setTagName(String tagName) {
        this.tagName = tagName;
        this.tagId = -1;
        return this;
    }

    /**
     * Returns the bundle tag name
     * @return
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * Returns the bundle id
     * @return
     */
    public long getBundleId() {
        return bundleId;
    }

    /**
     * Sets the bundle id. The bundle id is created by database!
     * @param bundleId - the bundle id
     * @return self
     */
    public Bundle setBundleId(long bundleId) {
        this.bundleId = bundleId;
        return this;
    }

    /**
     * Returns the bundle tag id
     * @return
     */
    public int getTagId() {
        return tagId;
    }

    /**
     * Sets the bundle tag id
     * @param tagId - the bundle tag id
     * @return
     */
    public Bundle setTagId(int tagId) {
        this.tagId = tagId;
        this.tagName = null;
        return this;
    }
    
    /**
     * Returns the bundle owner
     * @return
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the bundle owner
     * @param owner - user name
     * @return self
     */
    public Bundle setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Returns the bundle description
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the bundle description
     * @param description - text
     * @return self
     */
    public Bundle setDescription(String description) {
        this.description = description;
        return this;
    }
    
    /**
     * Sets the bundle properties hash map
     * @param properties - the hash map of properties
     * @return self
     */
    public Bundle setProperties(HashMap<String, String> properties) {
    	this.properties = properties;
    	return this;
    }
    
    /**
     * Returns the hash map of properties
     * @return HashMap
     */
    public HashMap<String, String> getProperties() {
    	return properties;
    }
    
    /**
     * Gets the bundle property indicated by the specified key
     * @param key - the name of the property
     * @return the string value of the bundle property, or null if there is no property with that key
     */
    public String getProperty(String key) {
    	return properties.get(key);
    }
    
    /**
     * Gets the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     * @return the string value of the bundle property, or null if there is no property with that key
     */
    public String getProperty(Enum<?> key) {
    	return properties.get(key.name());
    }

    /**
     * Sets the bundle property indicated by the specified key
     * @param key - the name of the property
     * @param value - the value of the property
     * @return self
     */
    public Bundle setProperty(String key, String value) {
    	properties.put(key, value);
    	return this;
    }
    
    /**
     * Sets the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     * @param value - the value of the property
     * @return self
     */
    public Bundle setProperty(Enum<?> key, String value) {
    	properties.put(key.name(), value);
    	return this;
    }
    
    /**
     * Removes the bundle property indicated by the specified key
     * @param key - the name of the property
     * @return self
     */
    public Bundle removeProperty(String key) {
        properties.remove(key);
        return this;
    }
    
    /**
     * Removes the bundle property indicated by the specified key
     * @param key - the enum value represents the name of the property
     * @return self
     */
    public Bundle removeProperty(Enum<?> key) {
        properties.remove(key.name());
        return this;
    }
    
    /**
     * Returns information about the bundle state (locked/unlocked)
     * @return
     */
    public boolean isLocked() {
    	return locked;
    }
    
    /**
     * Sets the bundle state (locked/unlocked)
     * @param locked - new state: true - locked, false - unlocked
     * @return self
     */
    public Bundle setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }
}
