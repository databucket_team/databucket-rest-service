package pl.databucket.api.springmvc.controller;
 
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.databucket.api.springmvc.dao.Bundle;
import pl.databucket.api.springmvc.dao.DatabucketService;
import pl.databucket.api.springmvc.exception.TableNotFoundException;

@RestController
public class RestServiceController {
	
	private enum InputParameter { USER_NAME, TABLE_NAME, TAG_NAME, TAG_ID, BUNDLE_ID, BUNDLES_IDS, DESCRIPTION, LOCK, PROPERTIES, COUNT}
	private final String NO_DATA = "No available data!";
	private final String NO_RESULT = "Something went wrong :/";
	private final String DONE = "Done";   
 
    @Autowired
    DatabucketService databucket;
    
    @RequestMapping(value = "api/CreateTable", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> CreateTable(@QueryParam("tableName") String tableName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
		boolean result = databucket.createTable(tableName);
		if (result)
			return ResponseEntity.status(HttpStatus.OK).body(DONE);
		else
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    }
    
    @RequestMapping(value = "api/DeleteTable", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addTable(@QueryParam("tableName") String tableName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
		boolean result = databucket.deleteTable(tableName);
		if (result)
			return ResponseEntity.status(HttpStatus.OK).body(DONE);
		else
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    }
    
    @RequestMapping(value = "api/AddBundleByTagId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addBundleByTagId(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId, @QueryParam("description") String description, @QueryParam("lock") Boolean lock, @RequestBody HashMap<String, String> properties) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	parameters.put(InputParameter.DESCRIPTION, description);
    	parameters.put(InputParameter.LOCK, lock);
    	parameters.put(InputParameter.PROPERTIES, properties);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, tagId);
    		bundle.setOwner(userName);
    		bundle.setDescription(description);
    		bundle.setLocked(lock);
    		bundle.setProperties(properties);
    		Long bundleId = databucket.addBundle(bundle);
    		if (bundleId > 0)
    			return new ResponseEntity<Long>(bundleId, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
    }

    @RequestMapping(value = "api/AddBundleByTagName", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addBundleByTagName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @QueryParam("description") String description, @QueryParam("lock") Boolean lock, @RequestBody HashMap<String, String> properties) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	parameters.put(InputParameter.DESCRIPTION, description);
    	parameters.put(InputParameter.PROPERTIES, properties);
    	parameters.put(InputParameter.LOCK, lock);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, tagName);
    		bundle.setOwner(userName);
    		bundle.setDescription(description);
    		bundle.setLocked(lock);
    		bundle.setProperties(properties);
    		Long bundleId = databucket.addBundle(bundle);
    		if (bundleId > 0)
    			return new ResponseEntity<Long>(bundleId, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
    }

	// Remove bundle
    @RequestMapping(value = "api/DeleteBundle", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteBundle(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("bundleId") Long bundleId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setOwner(userName);
    		boolean result = databucket.deleteBundleById(tableName, bundleId);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}  	
	}

    @RequestMapping(value = "api/GetBundleById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getBundleById(@QueryParam("tableName") String tableName, @QueryParam("bundleId") Long bundleId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = databucket.getBundleById(tableName, bundleId);
    		if (bundle != null)
    			return new ResponseEntity<Bundle>(bundle, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	} 
    
    @RequestMapping(value = "api/GetBundlesByIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getBundlesByIds(@QueryParam("tableName") String tableName, @QueryParam("bundlesIds") String bundlesIds) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLES_IDS, bundlesIds);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles = databucket.getBundlesByIds(tableName, bundlesIds);
    		if (bundles != null)
    			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}
    
    @RequestMapping(value = "api/GetBundlesByProperties", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getBundlesByProperties(@QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @RequestBody HashMap<String, String> properties) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	//parameters.put(InputParameter.TAG_NAME, tagName); // tag name could be null
    	parameters.put(InputParameter.PROPERTIES, properties);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles;
    		if (tagName == null)
    			bundles = databucket.getBundlesByProperties(tableName, properties);
    		else
    			bundles = databucket.getBundlesByProperties(tableName, tagName, properties);
    		
    		if (bundles != null)
    			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}    
    
    @RequestMapping(value = "api/GetTableBundles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getTableBundles(@QueryParam("tableName") String tableName) {
		
		// Verify input parameters
		Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    	
    
		try {
			List<Bundle> bundles = databucket.getTableBundles(tableName);
			if (bundles != null)
    			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
		} catch (TableNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}
    
    @RequestMapping(value = "api/GetTables", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getTables() {
		List<String> tables = databucket.getTables();
		if (tables != null)
			return new ResponseEntity<List<String>>(tables, HttpStatus.OK);
		else
			return new ResponseEntity<List<String>>(HttpStatus.NOT_FOUND);	
	}
		
    @RequestMapping(value = "api/GetTableTags", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTableTags(@QueryParam("tableName") String tableName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    	
    	List<String> tags = databucket.getTableTags(tableName);
		if (tags != null)
			return new ResponseEntity<List<String>>(tags, HttpStatus.OK);
		else
			return new ResponseEntity<List<String>>(HttpStatus.NOT_FOUND);
	}    
		
    @RequestMapping(value = "api/GetTagBundlesById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getTagBundlesById(@QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);    	
    	
    	try {
	    	List<Bundle> bundles = databucket.getTagBundles(tableName, tagId);
	    	if (bundles != null)
    			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
    		else
    			return new ResponseEntity<List<Bundle>>(HttpStatus.NOT_FOUND);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}
		
    @RequestMapping(value = "api/GetTagBundlesByName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> getTagBundlesByName(@QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);    	
    		
    	try {
    		List<Bundle> bundles = databucket.getTagBundles(tableName, tagName);
    		if (bundles != null)
    			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
    		else
    			return new ResponseEntity<List<Bundle>>(HttpStatus.NOT_FOUND);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    	}   	
	} 
    
    @RequestMapping(value = "api/LockBundleByTagId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> lockBundleByTagId(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = databucket.lockBundle(userName, tableName, tagId);
    		if (bundle != null)
    			return new ResponseEntity<Bundle>(bundle, HttpStatus.OK);
    		else
    			return new ResponseEntity<Bundle>(HttpStatus.NOT_FOUND);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
    	
	} 
		
    @RequestMapping(value = "api/LockBundleByTagName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<?> lockBundleByTagName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = databucket.lockBundle(userName, tableName, tagName);
    		if (bundle != null)
    			return new ResponseEntity<Bundle>(bundle, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	} 
    
    @RequestMapping(value = "api/LockBundlesByTagId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
   	public ResponseEntity<?> lockBundlesByTagId(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId, @QueryParam("count") Integer count) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	parameters.put(InputParameter.COUNT, count);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles = databucket.lockBundles(userName, tableName, tagId, count);
       		if (bundles != null)
       			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
       		else
       			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
   	}
		
    @RequestMapping(value = "api/LockBundlesByTagName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
   	public ResponseEntity<?> lockBundlesByTagName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @QueryParam("count") Integer count) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	parameters.put(InputParameter.COUNT, count);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles = databucket.lockBundles(userName, tableName, tagName, count);
       		if (bundles != null)
       			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
       		else
       			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
   	}
    
    @RequestMapping(value = "api/LockRandomBundleByTagId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
    public ResponseEntity<?> lockRandomBundleByTagId(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = databucket.lockRandomBundle(userName, tableName, tagId);
    		if (bundle != null)
    			return new ResponseEntity<Bundle>(bundle, HttpStatus.OK);
    		else
    			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}
    
    @RequestMapping(value = "api/LockRandomBundleByTagName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
   	public ResponseEntity<?> lockRandomBundleByTagName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = databucket.lockRandomBundle(userName, tableName, tagName);
       		if (bundle != null)
       			return new ResponseEntity<Bundle>(bundle, HttpStatus.OK);
       		else
       			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
   	}
    
    @RequestMapping(value = "api/LockRandomBundlesByTagId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
   	public ResponseEntity<?> lockRandomBundlesByTagId(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId, @QueryParam("count") Integer count) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	parameters.put(InputParameter.COUNT, count);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles = databucket.lockRandomBundles(userName, tableName, tagId, count);
       		if (bundles != null)
       			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
       		else
       			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
   	}
		
    @RequestMapping(value = "api/LockRandomBundlesByTagName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) 
   	public ResponseEntity<?> lockRandomBundlesByTagName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @QueryParam("count") Integer count) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	parameters.put(InputParameter.COUNT, count);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		List<Bundle> bundles = databucket.lockRandomBundles(userName, tableName, tagName, count);
       		if (bundles != null)
       			return new ResponseEntity<List<Bundle>>(bundles, HttpStatus.OK);
       		else
       			return ResponseEntity.status(HttpStatus.OK).body(NO_DATA);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
   	}	
	
    @RequestMapping(value = "api/SaveBundle", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveBundle(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @QueryParam("bundleId") Long bundleId, @QueryParam("description") String description, @RequestBody HashMap<String, String> properties) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	parameters.put(InputParameter.DESCRIPTION, description);
    	parameters.put(InputParameter.PROPERTIES, properties);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, tagName);
    		bundle.setBundleId(bundleId);
    		bundle.setOwner(userName);
    		bundle.setDescription(description);
    		bundle.setProperties(properties);
    		boolean result = databucket.saveBundle(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	} 	
	}
    
    @RequestMapping(value = "api/SaveBundleDescription", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveBundleDescription(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("bundleId") Long bundleId, @QueryParam("description") String description) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	parameters.put(InputParameter.DESCRIPTION, description);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setOwner(userName);
    		bundle.setDescription(description);
    		boolean result = databucket.saveBundleDescription(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}

    @RequestMapping(value = "api/SaveBundleProperties", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveBundleProperties(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("bundleId") Long bundleId, @RequestBody HashMap<String, String> properties) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	parameters.put(InputParameter.PROPERTIES, properties);
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setOwner(userName);
    		bundle.setProperties(properties);
    		boolean result = databucket.saveBundleProperties(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	} 	
	}
    
    @RequestMapping(value = "api/SaveBundleTagById", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveBundleTagById(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagId") Integer tagId, @QueryParam("bundleId") Long bundleId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_ID, tagId);
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setTagId(tagId);
    		bundle.setOwner(userName);
    		boolean result = databucket.saveBundleTagById(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);   	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}
	}
    
    @RequestMapping(value = "api/SaveBundleTagByName", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveBundleTagByName(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("tagName") String tagName, @QueryParam("bundleId") Long bundleId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.TAG_NAME, tagName);
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setTagName(tagName);
    		bundle.setOwner(userName);
    		boolean result = databucket.saveBundleTagByName(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}   	
	}	
		
	@RequestMapping(value = "api/UnlockBundle", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> unlockBundle(@QueryParam("userName") String userName, @QueryParam("tableName") String tableName, @QueryParam("bundleId") Long bundleId) {
    	
    	// Verify input parameters
    	Map<InputParameter, Object> parameters = new HashMap<InputParameter, Object>();
    	parameters.put(InputParameter.USER_NAME, userName);
    	parameters.put(InputParameter.TABLE_NAME, tableName); 
    	parameters.put(InputParameter.BUNDLE_ID, bundleId);    	
    	String message = verifyParameters(parameters);
    	if (message != null)
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message); 
    	
    	try {
    		Bundle bundle = new Bundle(tableName, bundleId);
    		bundle.setOwner(userName);
    		boolean result = databucket.unlockBundle(bundle);
    		if (result)
    			return ResponseEntity.status(HttpStatus.OK).body(DONE);    	
    		else
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(NO_RESULT);
    	} catch (TableNotFoundException e) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage()); 
    	}	
	}    
    
	private String verifyParameters(Map<InputParameter, Object> parameters) {
    	String message = null;
    	Iterator<Entry<InputParameter, Object>> it = parameters.entrySet().iterator();
    	while (it.hasNext()) {
    		Map.Entry<InputParameter, Object> pair = it.next();
    		switch (pair.getKey()) {
    		case USER_NAME:
    			String userName = (String) pair.getValue();
				if (userName == null || userName.length() <= 0) {
					if (message == null) message = "";
					message += "The parameter 'userName' (String) is required.\n";
				}
				break;
    		case TABLE_NAME:
    			String tableName = (String) pair.getValue();
				if (tableName == null || tableName.length() <= 0) {
					if (message == null) message = "";
					message += "The parameter 'tableName' (String) is required.\n";
				}
				break;
    		case TAG_NAME:
    			String tagName = (String) pair.getValue();
				if (tagName == null || tagName.length() <= 0) {
					if (message == null) message = "";
					message += "The parameter 'tagName' (String) is required.\n";
				}
				break;
    		case TAG_ID:
				Integer tagId = (Integer) pair.getValue();
				if (tagId <= 0) {
					if (message == null) message = "";
    				message += "The parameter 'tagId' (Integer) is required. It must be a number grater then 0.\n";
				}    					
				break;
			case DESCRIPTION:				
				break;
			case LOCK:				
				break;
			case PROPERTIES:				
				break;
			case BUNDLE_ID:
				Long bundleId = (Long) pair.getValue();
				if (bundleId == null || bundleId <= 0) {
					if (message == null) message = "";
    				message += "The parameter 'bundleId' (Long) is required. It must be a number grater then 0.\n";
				}
				break;
			case BUNDLES_IDS:
				String bundlesIds = (String) pair.getValue();
				if (bundlesIds.length() <= 0) {
					if (message == null) message = "";
					message += "The parameter 'bundlesIds' (String) is required. It must be a list of comma separated numbers grater then 0. Example: 3,4,30,45\n";
				} else {
					String[] ids = bundlesIds.trim().split(",");
					for (String id : ids) {
						try {
							Long l = Long.parseLong(id.trim());
							if (l <= 0) {
								if (message == null) message = "";
								message += "The parameter 'bundlesIds' (String) is required. It must be a list of comma separated numbers grater then 0. Example: 3,4,30,45\n";
								break;
							}
						} catch (NumberFormatException e) {
							if (message == null) message = "";
							message += "The parameter 'bundlesIds' (String) is required. It must be a list of comma separated numbers grater then 0. Example: 3,4,30,45\n";
							break;
						}
					}
				}
				break;
			case COUNT:
				Integer count = (Integer) pair.getValue();
				if (count <= 0) {
					if (message == null) message = "";
    				message += "The parameter 'count' (Integer) is required. It must be a number grater then 0.\n";
				}
				break;	  		
    		}
    	}
    	return message;
    }
}